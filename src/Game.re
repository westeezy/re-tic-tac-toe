type playerType =
  | Cross
  | Circle;

type gameStateType =
  | Playing
  | Won(playerType)
  | Tie;

type fieldType =
  | Empty
  | Filled(playerType);

type rowType = (fieldType, fieldType, fieldType);

type boardType = (rowType, rowType, rowType);

type rowIdType =
  | R1
  | R2
  | R3;

type colIdType =
  | C1
  | C2
  | C3;

/* Utility Methods for the Game */
let playerToString = player =>
  switch player {
  | Cross => "X"
  | Circle => "O"
  };

let playerToStatusString = player =>
  "Playing: " ++ playerToString(player) ++ "'s move.";

let updateField = (player, field) =>
  switch field {
  | Empty => Filled(player)
  | Filled(_) => field
  };

let updateRow = (player, cid, row) => {
  let (field1, field2, field3) = row;
  let updateField_ = updateField(player);
  switch cid {
  | C1 => (updateField_(field1), field2, field3)
  | C2 => (field1, updateField_(field2), field3)
  | C3 => (field1, field2, updateField_(field3))
  };
};

let isFull = (f, (f1, f2, f3)) => f == f1 && f1 == f2 && f2 == f3;

let hasWon = (player, (r1, r2, r3)) => {
  let f = Filled(player);
  let isFull_ = isFull(f);
  let (f11, f12, f13) = r1;
  let (f21, f22, f23) = r2;
  let (f31, f32, f33) = r3;
  isFull_(r1)
  || isFull_(r2)
  || isFull_(r3)
  || isFull_((f11, f21, f31))
  || isFull_((f12, f22, f32))
  || isFull_((f13, f23, f33))
  || isFull_((f11, f22, f33))
  || isFull_((f31, f22, f13));
};

let updateBoard = (player, rid, cid, board) => {
  let (row1, row2, row3) = board;
  let updateRow_ = updateRow(player, cid);
  switch rid {
  | R1 => (updateRow_(row1), row2, row3)
  | R2 => (row1, updateRow_(row2), row3)
  | R3 => (row1, row2, updateRow_(row3))
  };
};