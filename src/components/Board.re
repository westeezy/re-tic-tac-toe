open Game;

let component = ReasonReact.statelessComponent("Board");

let handleClick = s => Js.log(s);

let make = (~board, ~onUpdate, _children) => {
  ...component,
  render: (_) => {
    let (row1, row2, row3) = board;
    <div className="game-board">
      <Row rid=R1 row=row1 onUpdate />
      <Row rid=R2 row=row2 onUpdate />
      <Row rid=R3 row=row3 onUpdate />
    </div>;
  }
};