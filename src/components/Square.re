open Game;

let component = ReasonReact.statelessComponent("Square");

let pieceToString = piece =>
  switch piece {
  | Filled(Cross) => "X"
  | Filled(Circle) => "O"
  | Empty => ""
  };

let make = (~piece, ~onClick, ~rid, ~cid, _children) => {
  ...component,
  render: (_) =>
    <button className="square" onClick=(_evt => onClick(rid, cid))>
      (ReasonReact.stringToElement(pieceToString(piece)))
    </button>
};