open Game;

let component = ReasonReact.statelessComponent("Row");

let make = (~row, ~onUpdate, ~rid, _children) => {
  ...component,
  render: (_) => {
    let (r1, r2, r3) = row;
    <div className="board-row">
      <Square rid cid=C1 piece=r1 onClick=onUpdate />
      <Square rid cid=C2 piece=r2 onClick=onUpdate />
      <Square rid cid=C3 piece=r3 onClick=onUpdate />
    </div>;
  }
};