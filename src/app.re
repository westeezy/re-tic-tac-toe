[%bs.raw {|require('./app.css')|}];

open Game;

let namespace = "tic-tac-toe";

type action =
  | UpdateStatus(string)
  | UpdateBoard(rowIdType, colIdType)
  | Reset;

type state = {
  status: string,
  board: boardType,
  player: playerType,
  gameState: gameStateType
};

let initialState = {
  status: playerToStatusString(Cross),
  board: ((Empty, Empty, Empty), (Empty, Empty, Empty), (Empty, Empty, Empty)),
  player: Cross,
  gameState: Playing
};

let getInitialState = (_) => initialState;

let component = ReasonReact.reducerComponent("App");

let make = _children => {
  ...component,
  initialState: () => getInitialState(),
  reducer: (action, state) => {
    let {board, player, gameState} = state;
    switch action {
    | UpdateStatus(text) => ReasonReact.Update({...state, status: text})
    | UpdateBoard(row, column) =>
      if (gameState == Playing) {
        let newBoard = updateBoard(player, row, column, board);
        let isGameOver = hasWon(player, newBoard);
        let newStatus = isGameOver ? "Game Over" : playerToStatusString(player);

        ReasonReact.Update({
          ...state,
          board: newBoard,
          status: newStatus,
          player: player == Cross ? Circle : Cross,
          gameState:  isGameOver ? Won(player) : Playing
        });
      } else {
        ReasonReact.NoUpdate;
      }
    | Reset => ReasonReact.Update(getInitialState())
    };
  },
  render: ({state: {board, status}, send}) =>
  <div>
    <div className="toolbar">
      <div onClick=(_event => send(UpdateStatus("Changed")))>
        (ReasonReact.stringToElement(status))
      </div>
      <button onClick=(_event => send(Reset))>
        (ReasonReact.stringToElement("Reset"))
      </button>
    </div>
    <div className="game">
      <Board
        board
        onUpdate=((row, column) => send(UpdateBoard(row, column)))
      />
    </div>
  </div>
};